## NextDNS

Ich habe vor ein paar Tagen nach einer Alternative für [Pi-Hole](https://pi-hole.net/) gesucht - Pi-Hole ist ein richtig gutes Projekt, keine Frage - aber wenn ich mal nicht zu Hause bin, habe ich wieder Werbung. 😢

![NextDNS Homepage Screenshot](https://cdn.hashnode.com/res/hashnode/image/upload/v1609783308590/CHZMYWxnv.png)

[NextDNS](https://nextdns.io) ist ein öffentlicher DNS-Resolver.

Man kann [NextDNS](https://nextdns.io) mit und ohne Anmeldung nutzen.

Wenn man sich anmeldet, bekommt man Zugang zu Blocklists, Logs und andere Einstellungen.

## Features

### Bockierung von Domains
[NextDNS](https://nextdns.io) kann, wenn man sich anmeldet, Werbedomains blockieren.

Neben den Standard-Blocklisten wie z.B. Steven Black oder die Disconnect Listen bietet NextDNS eine hauseigene Liste an.

Mit NextDNS hat man auch die Möglichkeit Webseiten für Phishing mithilfe von eines "Threat-Intelligence-Feeds" das auf COVID-19 abziehlt zu blockieren.

Dinge die NextDNS mit speziellen Listen blockieren kann:

![Screenshot_2021-01-04 Sicherheit - FamFam - NextDNS.png](https://cdn.hashnode.com/res/hashnode/image/upload/v1609786999011/QcCXrH7d1.png)

## Statistiken

NextDNS bietet auch eine hilfreiche Statistik von allem was mit dem DNS Resolver aufgelöst wurde.

![Screenshot_2021-01-04 Statistiken - FamFam - NextDNS.png](https://cdn.hashnode.com/res/hashnode/image/upload/v1609787356376/YjXOiKVp2.png)

## Blockierseite

Was ich richtig cool finde, die blockierten Websites hören nicht einfach auf zu laden, sondern NextDNS zeigt dir eine Blockerseite:

![Screenshot_2021-01-04 getpaint net.png](https://cdn.hashnode.com/res/hashnode/image/upload/v1609787548527/SeZ5vRURt.png)

## Open Source

[NextDNS](https://github.com/nextdns) ist open source: https://github.com/nextdns