## awesomeBible Verse


> Versbilder leicht gemacht.

Ich habe awesomeBible Verse gestartet, weil ich eine API für Versbilder brauchte.

Meine ersten Versuche waren mit der YouVersion API, die mittlerweile aber keine neuen API-Keys mehr herausgibt.

Also habe ich meine eigene Versbild-API gebaut.

* * *
![](https://verse.awesomebible.de/)
* * *

Die Bilddateien befinden sich im selben GitHub Repository wie der Code (das sollte ich wahrscheinlich ändern). 

Der PHP-Code ist super simpel:


```PHP
<?php
	$DayOfYear = date('z') + 1;
	$cacheRef = "cacheFile.txt"; // Edit this to the exact file path for your install of verse + "cacheFile.txt"
    $cachedImage = "cachedImage.jpg"; // Edit this to the exact file path for your install of verse + "cachedImage.jpg"
    
    function cacheStale(){
        global $DayOfYear;
        global $cacheRef;
        global $cachedImage;

        // If cache is stale
			if(file_exists($cachedImage)){unlink($cachedImage);};
			if(file_exists($cacheRef)){unlink($cacheRef);};

			copy('https://raw.githubusercontent.com/awesomebible/verse/master/img/'.$DayOfYear.'.jpg', $cachedImage);

			header('Content-type: image/jpg;');
			header('Cache-Control: max-age 86400');
			echo file_get_contents($cachedImage);

			$myfile = fopen($cacheRef, "w") or die("Unable to open file!");
			$txt = "".$DayOfYear."\n";
			fwrite($myfile, $txt);
			fclose($myfile);
			die;
    };
    function cacheFresh(){
        global $cachedImage;
        	// If cache is fresh
			header('Content-type: image/jpg;');
			header('Cache-Control: max-age 86400');
            if(file_exists($cachedImage)){
			echo file_get_contents($cachedImage);
            die;
        }else{
            cacheStale();
        };
    };

    if(file_get_contents($cacheRef) == $DayOfYear){
        cacheFresh();
    }else{
        cacheStale();
    }

    echo "Sorry, but my creator forbid me to do that.";
    echo "<br>";
    echo "Error-Code: ".$DayOfYear."";
?>
```

1. Welcher Tag ist heute?

```PHP
$DayOfYear = date('z') + 1;
``` 

2. Wo liegen die einzelnen Dateien?
```PHP
$cacheRef = "cacheFile.txt"; // Edit this to the exact file path for your install of verse + "cacheFile.txt"
$cachedImage = "cachedImage.jpg"; // Edit this to the exact file path for your install of verse + "cachedImage.jpg"
``` 
 
3. Ist der Cache abgelaufen?

```PHP
    function cacheStale(){
        global $DayOfYear;
        global $cacheRef;
        global $cachedImage;

        // If cache is stale
			if(file_exists($cachedImage)){unlink($cachedImage);};
			if(file_exists($cacheRef)){unlink($cacheRef);};

			copy('https://raw.githubusercontent.com/awesomebible/verse/master/img/'.$DayOfYear.'.jpg', $cachedImage);

			header('Content-type: image/jpg;');
			header('Cache-Control: max-age 86400');
			echo file_get_contents($cachedImage);

			$myfile = fopen($cacheRef, "w") or die("Unable to open file!");
			$txt = "".$DayOfYear."\n";
			fwrite($myfile, $txt);
			fclose($myfile);
			die;
    };
```

- Hol dir die wichtigen Variablen rein.
- Lösche das gecachte Bild und die Datei die die Cache-Zeit beinhaltet. (mit 
```
unlink()
```)
- Lade die neue Datei von Github runter, setzte die entsprechenden Content-Type und Caching-Header und sende das Bild an den Client.

4. Ist der Cache nicht abgelaufen?


```PHP
function cacheFresh(){
        global $cachedImage;
        	// If cache is fresh
			header('Content-type: image/jpg;');
			header('Cache-Control: max-age 86400');
            if(file_exists($cachedImage)){
			echo file_get_contents($cachedImage);
            die;
        }else{
            cacheStale();
        };
    };
```
- Hol dir die wichtigen Variablen rein.
- Setze die entsprechenden Content-Type und Cache-Control Header.
- Gib die Datei aus.

Das ist der gesamte Code der Haupt-Anwendung.