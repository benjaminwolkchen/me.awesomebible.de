## unDraw

Auf  [awesomeBible](https://awesomebible.de) benutze ich an einer Stelle diese Illustration:

![undraw_Letter_re_8m03.png](https://cdn.hashnode.com/res/hashnode/image/upload/v1608299436255/uh1DU_umB.png)

Dieses Bild ist von [unDraw](https://undraw.co).

unDraw sind open source Illustrationen.

## Wie kann Bild open-source sein?

Für eine Illustration gibt es ja keinen Sourcecode...

Nein, aber das was eine open-source Lizenz auch mitbringt ist die freie Verwendung.
Hierauf wird sich bei unDraw fokussiert.

Die ganze Lizenz ist [hier](https://undraw.co/license) zu finden.

Ein kurzer Ausschnitt:

> If you are working on something and want to use illustrations to improve its appearance, modified or not, without the need for attribution or cost, you are good to go. If you find unDraw or its illustrations to be in the center of what you are doing (e.g. sell or re-distribute one/some of them, add them in an app), then you probably should not proceed.

### Happy (un)Drawing!