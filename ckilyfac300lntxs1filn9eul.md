## my.awesomeBible: Ein Blick hinter die Kulissen

Ich nutze einige Bibelapps (MyBible, Die-Bibel.de, Bibleserver, Die Bibel von YouVersion) aber eins hat mir immer gefehlt: Einen Ort wo ich direkt Notizen machen kann, und zwar mit einem ordentlichen Editor, am besten in Markdown.

Und weil programmieren so ein Hobby von mir ist, habe ich einfach mal angefangen.

* * *
## Warum eine weitere Bibel-App?

Die Idee hinter my.awesomeBible ist einen Platz für Gedanken zu schaffen. Einen Ort wo man gerne aufschreibt, was einem einfällt - weil es einem Spaß macht.

* * *
## Design

Aktuell ist das Design wirklich simpel.

Ein Background-Gradient, und ein Boxed-Layout.
Noch ein bisschen CSS Sprinkles und schon sind die Links fancy.

![my.awesomeBible Startseite](https://cdn.hashnode.com/res/hashnode/image/upload/v1607789107297/blEKtI-Go.png?w=1600&h=840&fit=crop&crop=entropy&auto=format&q=60)

* * * 
## Das Problem mit der Zeit
Ich bin was neue Dinge angeht, extrem ungeduldig.
Wenn ich etwas ansatzweise fertig habe, möchte ich es immer sofort releasen.
Ich kann bei sowas einfach nicht warten.

Für my.awesomeBible habe ich mir vorgenommen, ganz anders vorzugehen.
Ich werde mir für my.awesomeBible kein Zeitlimit setzten, ich will aber auch nicht ein unfertiges Projekt veröffentlichen.

Keine Sorge, der Sourcecode ist auf  [Github](https://github.com/awesomebible/my)  - my.awesomeBible ist, und wird immer open source bleiben.

%[https://github.com/awesomebible/my]

Weiter im Text: Ach ja, das mit der Zeit.

Ich werde mir für my.awesomeBible richtig viel Zeit nehmen, solange bis es ein fertiges Produkt ist.

Ja, Produkt. Ich will nicht wieder dieses Mindset von "Nebenprojekt" bekommen, wenn ich an my.awesomeBible arbeite.

Ich will wirklich ein Produkt veröffentlichen, dass von Menschen genutzt werden kann.

Ich möchte nicht, dass der erste Eindruck irgendwie negativ ist.

Ich möchte die Nutzer von my.awesomeBible fröhlich machen, und ein gutes Produkt für eine "Marktlücke" produzieren.

## Die DNA
Ich habe jetzt viel über my.awesomeBible als Produkt geredet, und Produkte kosten in der Regel Geld.

Ich finde das ist ein wichtiges Thema, denn ich glaube es gibt 3 Dinge die in der DNA von awesomeBible stehen:

- Es geht um Gott, zu seinem Lobpreis, zu seiner Ehre.
- Es geht um die Menschen, ihnen Gottes geniale Story zu erzählen.
- Nicht für Profit. Es geht um Gott, nicht um Geld.

Klar muss es irgendeinen Weg geben, wie sich my.awesomeBible finanziert.
Das werden am Ende wahrscheinlich zwei Dinge sein:

### 1. Mein eigenes Geld & Spenden
Ich weiß nicht, ob es jemals so weit kommen wird, dass irgendjemand mir etwas spendet.

Wenn es soweit kommen sollte, könnt ihr euch sicher sein, dass das Geld zu 100% wieder in awesomeBible investiert wird.

awesomeBible (DE / EN / Verse) werden aktuell komplett aus eigener Tasche finanziert. (Danke Mama und Papa, **ihr seid super**! ❤️)

### 2. *optionale* Premiumfeatures
Wichtig hier: optional.
Ich werde **niemals** Core-Features hinter einer Paywall verstecken.

Hiermit habt ihr's schriftlich. Ihr könnt euch das gerne auch noch ausdrucken. Im  [Web Archive](http://web.archive.org/web/20201212172603/https://me.awesomebible.de/my-one?guid=none&deviceId=a7e54587-b790-40c4-aedb-cfe03bec55d0)  ist es auch archiviert.

## Wie geht's weiter?
Wenn ich es geschafft habe, Authentifizierung zu implementieren ist meine Roadmap fürs Erste wieder leer. (Normalerweise brauche ich nicht lange, bis mir etwas neues einfällt.)

Falls du eine Idee für my.awesomeBible hast, schreib sie mir.
Ernsthaft. Ich will wissen, was du brauchst um dich in deiner Bibel-App wohl zu fühlen.

Sei es, dass du einen Dark-Mode brauchst. (Okay, das brauchst du nicht mehr fragen, das Feature ist bereits auf der Roadmap. lol.)

Wenn du eine Idee hast, oder vielleicht hast du noch keine und willst einfach wissen wie es mir geht:

Schreib mir. 😊 Ich freue mich wirklich über jede Nachricht.

E-Mail:  [benjamin@awesomebible.de](mailto:benjamin@awesomebible.de)

Ich glaube, ich mache so "Devlog" artige Artikel öfters, das macht mir echt Spaß.

Bis dahin,
Benjamin.