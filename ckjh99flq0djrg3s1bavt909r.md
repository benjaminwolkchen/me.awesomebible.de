## Die BibleProject App

Die App vom BibleProject ist am ersten Januar für Android und iOS erschienen.


![365 x BibleProject - Deutsch Appstore Screenshot](https://cdn.hashnode.com/res/hashnode/image/upload/v1609683326041/UYuydG4n5.png)

* * *

Die App ist ein Leseplan - in einem Jahr durch die gesamte Bibel. 📘


![startup.png](https://cdn.hashnode.com/res/hashnode/image/upload/v1609684793061/6GM6JuVDF.png)

Die App begrüßt einen mit einer kleinen Einleitung:

![intro1.png](https://cdn.hashnode.com/res/hashnode/image/upload/v1609684972073/53Gq7wdA1.png)

![intro2.png](https://cdn.hashnode.com/res/hashnode/image/upload/v1609684946521/q6Lmj7fEj.png)

![intro3.png](https://cdn.hashnode.com/res/hashnode/image/upload/v1609684993646/5DwTSF02g.png)

 * * * 
Der Bibeltext ist verständlich erklärt, innerhalb der Abschnitte gibt es manchmal Aufrufe zum Gebet, oder andere "Call-to-Action"'s:

![calltoaction1.png](https://cdn.hashnode.com/res/hashnode/image/upload/v1609685200538/0oNmxxIMj.png)

![calltoaction2.png](https://cdn.hashnode.com/res/hashnode/image/upload/v1609685225346/S88mnIG2Z.png)

Ich werde die App jetzt erst einmal über längere Zeit ausprobieren.

Die App für Android: https://play.google.com/store/apps/details?id=org.visiomedia.bibleprojectdeutsch

Und für iOS: https://apps.apple.com/us/app/365-x-bibleproject-deutsch/id1544958885